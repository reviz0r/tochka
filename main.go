package main

import (
	"log"
	"os"
	"os/signal"
	"tochka/model"
	"tochka/parser"
	"tochka/routes"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	// Запускаем обработку завершения приложения
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)
	go handleExit(c)

	// Запускаем сервер
	var e = echo.New()

	e.Use(middleware.CORS())
	e.Use(middleware.Logger())

	var api = e.Group("/api")
	api.GET("/posts", routes.PostHandler)
	api.POST("/start", routes.StartHandler)

	err := e.Start(":" + os.Getenv("PORT"))
	log.Fatal(err)
}

func handleExit(c chan os.Signal) {
	<-c
	close(parser.Done)
	parser.ParserWg.Wait()
	model.CloseConnection()
	log.Println("Server stopped")
	os.Exit(0)
}
