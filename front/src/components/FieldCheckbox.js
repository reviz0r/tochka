import React from 'react'
import PropTypes from 'prop-types'
import { FormGroup, Label, Input } from 'reactstrap'

const FieldCheckbox = ({ id, label, bsSize, ...props }) => {
  return (
    <FormGroup>
      <Label size={bsSize} for={id}>
        <Input type='checkbox' bsSize={bsSize} id={id} {...props} />
        {' ' + label}
      </Label>
    </FormGroup>
  )
}

FieldCheckbox.propTypes = {
  id: PropTypes.string,
  bsSize: PropTypes.string,
  label: PropTypes.string
}

export default FieldCheckbox
