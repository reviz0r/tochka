import React, { Component } from 'react'
import autoBind from 'react-autobind'
import { Button, Col, Form, Row } from 'reactstrap'

import Field from './Field'
import FieldCheckbox from './FieldCheckbox'

import postForm from '../api/form'

class MainForm extends Component {
  constructor (props) {
    super(props)
    autoBind(this)
    this.state = { isRss: true }
  }

  handleRss (evt) {
    const target = evt.target
    this.setState({ isRss: target.checked })
  }

  handleSubmit (evt) {
    evt.preventDefault()
    const target = evt.target
    let value = {
      url: target.url.value,
      rss: target.rss.checked
    }
    if (!this.state.isRss) {
      value = {
        ...value,
        container: target.containerSelector.value,
        title: target.title.value,
        titleAttr: target.titleAttr.value,
        description: target.description.value,
        descriptionAttr: target.descriptionAttr.value,
        link: target.link.value,
        linkAttr: target.linkAttr.value,
        published: target.published.value,
        publishedAttr: target.publishedAttr.value
      }
    }
    postForm(value)
  }

  render () {
    const { isRss } = this.state
    return (
      <Form onSubmit={this.handleSubmit}>
        <Field
          id='feed-url'
          name='url'
          type='url'
          label='Адрес новостного источника'
        />
        <FieldCheckbox
          id='rss-toggle'
          name='rss'
          label='RSS'
          checked={isRss}
          onChange={this.handleRss}
        />
        {
          !isRss &&
          <div>
            <Field
              id='container-selector'
              name='containerSelector'
              type='text'
              bsSize='sm'
              label='Селектор контейнера новости'
            />
            <Row>
              <Col>
                <Field
                  id='title-selector'
                  name='title'
                  type='text'
                  bsSize='sm'
                  label='Селектор заголовка новости'
                />
              </Col>
              <Col>
                <Field
                  id='title-attr'
                  name='titleAttr'
                  type='text'
                  bsSize='sm'
                  label='Атрибут заголовка новости'
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  id='description-selector'
                  name='description'
                  type='text'
                  bsSize='sm'
                  label='Селектор описания новости'
                />
              </Col>
              <Col>
                <Field
                  id='description-attr'
                  name='descriptionAttr'
                  type='text'
                  bsSize='sm'
                  label='Атрибут описания новости'
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  id='link-selector'
                  name='link'
                  type='text'
                  bsSize='sm'
                  label='Селектор ссылки на новость'
                />
              </Col>
              <Col>
                <Field
                  id='link-attr'
                  name='linkAttr'
                  type='text'
                  bsSize='sm'
                  label='Атрибут ссылки на новость'
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  id='published-selector'
                  name='published'
                  type='text'
                  bsSize='sm'
                  label='Селектор времени публикации новости'
                />
              </Col>
              <Col>
                <Field
                  id='published-attr'
                  name='publishedAttr'
                  type='text'
                  bsSize='sm'
                  label='Атрибут времени публикации новости'
                />
              </Col>
            </Row>
          </div>
        }
        <Button type='submit' color='primary'>Отправить</Button>
      </Form>
    )
  }
}

export default MainForm
