import React, { Component } from 'react'
import autoBind from 'react-autobind'
import {
  Button,
  Card,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Col,
  Row
} from 'reactstrap'

import Field from './Field'

import getPosts from '../api/posts'

class FeedList extends Component {
  constructor (props) {
    super(props)
    autoBind(this)
    this.state = {
      search: '',
      posts: []
    }
  }

  componentDidMount () {
    getPosts()
      .then(posts => this.setState({ posts }))
  }

  handleSearchChange (evt) {
    const target = evt.target
    const search = target.value
    getPosts(search)
      .then(posts => this.setState({ search, posts }))
  }

  formatDate (date) {
    return new Date(date).toLocaleString()
  }

  render () {
    return (
      <div>
        <Row>
          <Col>
            <Field
              id='feed-search'
              name='search'
              type='search'
              bsSize='sm'
              placeholder='Поиск...'
              value={this.state.search}
              onChange={this.handleSearchChange}
            />
          </Col>
        </Row>
        <Row>
          {
            this.state.posts.map(
              post => (
                <Col key={post.id} md={4} style={{ padding: 15 }}>
                  <Card>
                    <CardBody>
                      <CardTitle>{post.title}</CardTitle>
                      <CardSubtitle className='text-muted'>{this.formatDate(post.published)}</CardSubtitle>
                      <CardText>{post.description}</CardText>
                      <Button onClick={() => window.open(post.link)}>Читать далее</Button>
                    </CardBody>
                  </Card>
                </Col>
              )
            )
          }
        </Row>
      </div>
    )
  }
}

export default FeedList
