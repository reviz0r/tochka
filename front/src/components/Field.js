import React from 'react'
import PropTypes from 'prop-types'
import { FormGroup, Label, Input } from 'reactstrap'

const Field = ({ id, label, bsSize, ...props }) => {
  return (
    <FormGroup>
      {
        label &&
        <Label size={bsSize} for={id}>{label}</Label>
      }
      <Input bsSize={bsSize} id={id} {...props} />
    </FormGroup>
  )
}

Field.propTypes = {
  id: PropTypes.string,
  bsSize: PropTypes.string,
  label: PropTypes.string
}

export default Field
