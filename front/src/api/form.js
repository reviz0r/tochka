import axios from 'axios'

const {
  REACT_APP_SERVER,
  REACT_APP_START
} = process.env

export default formData => axios
  .post(REACT_APP_SERVER + REACT_APP_START, formData)
  .then(res => res.data)
