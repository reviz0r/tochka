import axios from 'axios'

const {
  REACT_APP_SERVER,
  REACT_APP_POSTS
} = process.env

export default search => axios
  .get(REACT_APP_SERVER + REACT_APP_POSTS, { params: { search } })
  .then(res => res.data)
