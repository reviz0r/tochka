import React, { Component } from 'react'
import { Card, CardBody, Container, Col, Row } from 'reactstrap'

import MainForm from './components/MainForm'
import FeedList from './components/FeedList'

class App extends Component {
  render () {
    return (
      <Container style={{ marginTop: 30, marginBottom: 30 }}>
        <Row>
          <Col>
            <Card>
              <CardBody>
                <MainForm />
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row style={{ marginTop: 30 }}>
          <Col>
            <Card>
              <CardBody>
                <FeedList />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default App
