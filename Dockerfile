FROM golang:1.9

WORKDIR /go/src/tochka
COPY . .

RUN go get
RUN go build
CMD [ "tochka" ]
