package model

import (
	"encoding/json"
	"log"
	"time"

	"github.com/mmcdole/gofeed"
)

// Post Новостной пост
type Post struct {
	ID          uint       `json:"id" gorm:"primary_key"`
	Title       string     `json:"title" gorm:"unique"`
	Description string     `json:"description"`
	Link        string     `json:"link"`
	Published   *time.Time `json:"published"`
	From        string     `json:"from"`
}

// GetPosts Получить посты
func GetPosts(search string) []Post {
	var posts []Post
	if len(search) != 0 {
		db.Where("title ILIKE ?", "%"+search+"%").Find(&posts)
	} else {
		db.Find(&posts)
	}
	return posts
}

// Save Сохранить пост
func (p Post) Save() {
	db.Save(&p)
}

func (p Post) String() string {
	var jsonStr, err = json.Marshal(p)
	if err != nil {
		log.Println("Ошибка при преобразоватии Post в JSON | ", err)
		return ""
	}
	return string(jsonStr)
}

// PostFromFeed Создать пост
func PostFromFeed(item *gofeed.Item, from string) Post {
	return Post{
		Title:       item.Title,
		Description: item.Description,
		Link:        item.Link,
		Published:   item.PublishedParsed,
		From:        from}
}

// SavePosts Сохранить список постов
func SavePosts(posts []Post) {
	for _, post := range posts {
		post.Save()
	}
}
