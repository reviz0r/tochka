package model

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	// Подключение диалекта postgres
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB

func init() {
	var err error
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	// Подключение к базе
	db, err = gorm.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal("Ошибка при соединении с БД | ", err)
	}
	log.Println("Успешное подключение к БД")

	// Включение логгера
	// db.LogMode(true)

	// Создание таблиц
	db.DropTableIfExists(&Post{})
	db.AutoMigrate(&Post{})
}

// CloseConnection Закрывает соединение с БД
func CloseConnection() {
	var err = db.Close()
	if err != nil {
		log.Fatal("Ошибка при закрытии соединения с БД | ", err)
	}
}
