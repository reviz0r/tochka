# Тестовое задание для банка "Точка"

## Запуск

Приложение запускается командой:
```bash
docker-compose up -d
```

Запускается 4 контейнера:
- Основное приложение
- Front-end для приложения
- База данных (Postgresql)
- Adminer (просмотр базы)

## Парсинг HTML

### Wylsa.com (https://wylsa.com/)

```golang
Container:     ".mnky-posts.ajax-load-posts .mp-container"
Title:         ".mp-inner-container .mp-content-container .mp-header .mp-title a span"
Description:   ".mp-inner-container .mp-content-container .mp-excerpt p"
Link:          ".mp-inner-container .mp-image-url"
LinkAttr:      "href"
Published:     ".mp-inner-container .mp-content-container .mp-article-meta .mp-date time[itemprop=datePublished]"
PublishedAttr: "datetime"
```

### Geekbrains (https://geekbrains.ru/posts)

```golang
Container:     ".post-items .post-item",
Title:         ".post-item__title",
Description:   ".post-link__content .post-link__text",
Link:          "a",
LinkAttr:      "href",
Published:     ".post-link__content .post-link__footer .post-link__day"
```

## Парсинг RSS

Дополнительных настроек не требуется

### Linux.org.ru (https://www.linux.org.ru/section-rss.jsp?section=1)

### Opennet.ru (http://www.opennet.ru/opennews/opennews_all.rss)
