package routes

import (
	"tochka/parser"

	"github.com/labstack/echo"
)

// StartRequest Формат запроса
type StartRequest struct {
	URL string `json:"url" form:"url"`
	RSS bool   `json:"rss" form:"rss"`
	parser.PostSelector
}

// StartHandler Обработчик роута /start
func StartHandler(ctx echo.Context) error {
	var request = new(StartRequest)
	ctx.Bind(request)

	var ps = parser.PostSelector{
		Container:       request.Container,
		Title:           request.Title,
		TitleAttr:       request.TitleAttr,
		Description:     request.Description,
		DescriptionAttr: request.DescriptionAttr,
		Link:            request.Link,
		LinkAttr:        request.LinkAttr,
		Published:       request.Published,
		PublishedAttr:   request.PublishedAttr}

	// Создаем парсер
	var parser = parser.Parser{
		URL:       request.URL,
		RSS:       request.RSS,
		Selectors: ps,
		Done:      parser.Done}

	// Запускаем обработку
	go parser.Start()

	return nil
}
