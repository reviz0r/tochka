package routes

import (
	"net/http"
	"tochka/model"

	"github.com/labstack/echo"
)

// PostHandler Обработчик корневого роута
func PostHandler(ctx echo.Context) (err error) {
	var search = ctx.QueryParam("search")

	// Отдаем список постов
	var posts = model.GetPosts(search)
	ctx.JSON(http.StatusOK, posts)
	return nil
}
