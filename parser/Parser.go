package parser

import (
	"sync"
	"time"
	"tochka/model"
)

// Done Канал для завершения работы
var Done = make(chan struct{}, 1)

// ParserWg Группа ожидания
var ParserWg sync.WaitGroup

// Parser Парсер новостей
type Parser struct {
	URL       string
	RSS       bool
	Selectors PostSelector
	Done      chan struct{}
}

// Start Получить посты
func (p Parser) Start() {
	ParserWg.Add(1)
	defer ParserWg.Done()
	// Получение обработчика
	var handler = p.getHandler()
	// Обработка и сохранение постов
	var posts = handler(p.URL)
	model.SavePosts(posts)
	// Старт таймера обновления
	tick := time.Tick(1 * time.Minute)
	for {
		select {
		case <-tick:
			// Обработка и сохранение постов
			var posts = handler(p.URL)
			model.SavePosts(posts)
		case <-p.Done:
			return
		}
	}
}

func (p Parser) getHandler() func(url string) []model.Post {
	var postHandler func(url string) []model.Post
	if p.RSS {
		postHandler = handleRssPosts
	} else {
		postHandler = handleHTMLPosts(p.Selectors)
	}
	return postHandler
}
