package parser

import (
	"log"
	"tochka/model"

	"github.com/PuerkitoBio/goquery"
)

func handleHTMLPosts(ps PostSelector) func(url string) []model.Post {
	return func(url string) []model.Post {
		log.Println("Чтение постов в сайта ", url)
		var doc, err = goquery.NewDocument(url)
		if err != nil {
			log.Fatal("Ошибка при чтении постов с ", url, " | ", err)
		}

		var posts = ps.GetPosts(doc)
		return posts
	}
}
