package parser

import (
	"time"
	"tochka/model"

	"github.com/PuerkitoBio/goquery"
)

// PostSelector формат парсинга постов
type PostSelector struct {
	Container       string `json:"container" form:"container"`
	Title           string `json:"title" form:"title"`
	TitleAttr       string `json:"titleAttr" form:"titleAttr"`
	Description     string `json:"description" form:"description"`
	DescriptionAttr string `json:"descriptionAttr" form:"descriptionAttr"`
	Link            string `json:"link" form:"link"`
	LinkAttr        string `json:"linkAttr" form:"linkAttr"`
	Published       string `json:"published" form:"published"`
	PublishedAttr   string `json:"publishedAttr" form:"publishedAttr"`
}

// GetPosts получить посты из документа
func (ps PostSelector) GetPosts(doc *goquery.Document) (posts []model.Post) {
	doc.Find(ps.Container).Each(func(i int, s *goquery.Selection) {
		title := getParam(s, ps.Title, ps.TitleAttr)
		description := getParam(s, ps.Description, ps.DescriptionAttr)
		link := getParam(s, ps.Link, ps.LinkAttr)
		published := getParam(s, ps.Published, ps.PublishedAttr)
		publishedAt, _ := time.Parse(time.RFC3339, published)
		var post = model.Post{
			Title:       title,
			Description: description,
			Link:        link,
			Published:   &publishedAt,
			From:        doc.Url.String()}
		posts = append(posts, post)
	})
	return posts
}

func getParam(s *goquery.Selection, selector string, attr string) string {
	if selector == "" && attr != "" {
		var param, _ = s.Attr(attr)
		return param
	}
	if selector != "" && attr == "" {
		return s.Find(selector).Text()
	}
	var param, _ = s.Find(selector).Attr(attr)
	return param
}
