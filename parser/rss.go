package parser

import (
	"log"
	"tochka/model"

	"github.com/mmcdole/gofeed"
)

func handleRssPosts(url string) (posts []model.Post) {
	log.Println("Чтение постов с RSS ", url)
	parser := gofeed.NewParser()
	feed, err := parser.ParseURL(url)
	if err != nil {
		log.Fatal("Ошибка при чтении постов с ", url, " | ", err)
	}

	for _, item := range feed.Items {
		var post = model.PostFromFeed(item, feed.Link)
		posts = append(posts, post)
	}
	return posts
}
